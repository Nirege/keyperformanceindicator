Entidad: KeyPerformanceIndicator 
===========================
  

[Licencia abierta](https://github.com/smart-data-models/dataModel.KeyPerformanceIndicator/blob/master/keyPerformanceIndicator/LICENSE.md)  

Descripción global: **Los indicadores KPI se utilizan para representar mediciones de rendimiento.**  

Utilizar los estándares y mejores prácticas definidas [en este link](https://gitlab.com/vlci-public/models-dades/estandares/-/blob/main/README.md).

# Lista de propiedades obligatorias
### Identificación de la entidad
- `id`: Identificador único de la entidad.
- `type`: NGSI Tipo de entidad. (En este caso KeyPerformanceIndicator)
### Atributos que se definen y se les da valor una única vez
- `calculationFrequency`: Con qué frecuencia se calcula el KPI. Posibles valores: horario, diario, semanal, mensual, anual, trimestral, bimestral, quincenal,etc.
- `definitionOwner`: Responsable de la definición del indicador
- `description`: Descripción de esta entidad.
- `isKPIFraction`: true / false. True indica que el KPI se obtiene en base a un numerador y un denominador. False indica que el KPI es un valor concreto, que no necesita de una división para calcularse. Cuando este campo sea true, obligatoriamente se tiene que tener valor en los campos kpiNumeratorValue y kpiDenominatorValue.
- `kpiDenominatorDescription`: Descripción de qué mide el numerador utilizado para la obtención del valor del KPI.  Obligatorio en caso de que el KPI se calcule mediante una división entre dos valores, por ejemplo un % o un promedio.
- `kpiNumeratorDescription`: Descripción de qué mide el numerador utilizado para la obtención del valor del KPI.  Obligatorio en caso de que el KPI se calcule mediante una división entre dos valores, por ejemplo un % o un promedio.
- `maintenanceOwner`: Responsable técnico de esta entidad.
- `maintenanceOwnerEmail`: Email del responsable técnico de esta entidad.
- `measureUnit`: Nombre de la unidad de medida.
- `name`: El nombre de este artículo.
- `organization`: Organización que evalua el KPI.
- `project`: Proyecto (para el proyecto impulso su valor es impulsoVLCI-red.es)
- `serviceName`: Nombre del servicio.
- `serviceOwner`: Persona del servicio municipal de contacto para esta entidad.
- `serviceOwnerEmail`: Email de la persona del servicio municipal de contacto para esta entidad.
- `sliceAndDice1`: Primer criterio de desagregación. Puede ser vacío, pero el atributo debe existir.
- `sliceAndDice2`: Segundo criterio de desagregación. Puede ser vacío, pero el atributo debe existir.
- `sliceAndDice3`: Tercer criterio de desagregación. Puede ser vacío, pero el atributo debe existir.
- `sliceAndDice4`: Cuarto criterio de desagregación. Puede ser vacío, pero el atributo debe existir.
### Atributos que varían en cada envío
- `calculationPeriod`: Periodo temporal en el que se ha realizado la medición. En otras palabras, dado una calculationFrequency = anual, el valor concreto, ej: 2022.
- `kpiDenominatorValue`: Número denominador utilizado para la obtención del valor del KPI. Obligatorio en caso de que el KPI se calcule mediante una división entre dos valores, por ejemplo un % o un promedio. (Si tiene decimales, utilizar el . como separador decimal)
- `kpiNumeratorValue`: Número numerador utilizado para la obtención del valor del KPI.  Obligatorio en caso de que el KPI se calcule mediante una división entre dos valores, por ejemplo un % o un promedio. (Si tiene decimales, utilizar el . como separador decimal)
- `kpiValue`: Valor del indicador. Puede ser de cualquier tipo. (Si tiene decimales, utilizar el . como separador decimal)
- `operationalStatus`: Posibles valores: ok, noData. Funcionamiento normal: en cada envío establecer ese atributo a ok. Internamente, la plataforma de ciudad establecerá el valor noData en caso de pasar un X tiempo sin recibir datos del indicador.
- `sliceAndDiceValue1`: Valor del primer criterio de desagregación. (Si tiene decimales, utilizar el . como separador decimal). Puede ser vacío, pero el atributo debe existir.
- `sliceAndDiceValue2`: Valor del segundo criterio de desagregación. (Si tiene decimales, utilizar el . como separador decimal). Puede ser vacío, pero el atributo debe existir.
- `sliceAndDiceValue3`: Valor del tercer criterio de desagregación. (Si tiene decimales, utilizar el . como separador decimal). Puede ser vacío, pero el atributo debe existir.
- `sliceAndDiceValue4`: Valor del cuarto criterio de desagregación. (Si tiene decimales, utilizar el . como separador decimal). Puede ser vacío, pero el atributo debe existir.
- `updatedAt`: Fecha y hora en la que se actualizó el valor de la entidad.  

# Lista de propiedades opcionales
- `calculationFormula`:
- `category`: Categoría de indicador. Posibles valores: quantitative, qualitative, leading, lagging, etc.
- `censusTract`: Sección censal
- `censusTractId`: Identificador de Sección censal
- `desiredTrend`: 
- `district`: Distrito.
- `districtId`: Id del distrito.
- `location`: Referencia Geojson al elemento. Puede ser Point, LineString, Polygon, MultiPoint, MultiLineString o MultiPolygon.
- `lowerThreshold`: Umbral inferior (Si tiene decimales, utilizar el . como separador decimal)
- `neighborhood`: Barrio.
- `neighborhoodId`: Id del barrio.
- `observations`: 
- `processName`: 
- `product`: Hay que definir el proceso o el producto.
- `provider`: Proveedor del producto o servicio, si lo hay, que este KPI evalúa.
- `references`: 
- `smartDimension`: 
- `source`: Una secuencia de caracteres que indica la fuente original de los datos de la entidad en forma de URL. Se recomienda que sea el nombre de dominio completo del proveedor de origen, o la URL del objeto de origen
- `systemType`: 
- `technology`: 
- `upperThreshold`: Umbral superior (Si tiene decimales, utilizar el . como separador decimal)

## Atributos Multiidioma (Si se tienen, publicarlos)
- `calculationFormulaCas`:
- `calculationFormulaVal`:
- `calculationFrequencyCas`: Con qué frecuencia se calcula el KPI. Posibles valores: horario, diario, semanal, mensual, anual, trimestral, bimestral, quincenal,etc. El idioma es castellano.
- `calculationFrequencyVal`: Con qué frecuencia se calcula el KPI. Posibles valores: horario, diario, semanal, mensual, anual, trimestral, bimestral, quincenal,etc. El idioma es valenciano.
- `descriptionCas`: Descripción de esta entidad en castellano.
- `descriptionVal`: Descripción de esta entidad en valenciano.
- `measureUnitCas`: Unidad de medida en castellano.
- `measureUnitVal`: Unidad de medida en valenciano.
- `nameCas`: El nombre de este artículo en castellano.
- `nameVal`: El nombre de este artículo en valenciano.
- `sliceAndDice1Cas`: Primer criterio de desagregación en castellano.
- `sliceAndDice1Val`: Primer criterio de desagregación en valenciano.
- `sliceAndDice2Cas`: Segundo criterio de desagregación en castellano.
- `sliceAndDice2Val`: Segundo criterio de desagregación en valenciano.
- `sliceAndDice3Cas`: Tercer criterio de desagregación en castellano.
- `sliceAndDice3Val`: Tercer criterio de desagregación en valenciano.
- `sliceAndDice4Cas`: Cuarto criterio de desagregación en castellano.
- `sliceAndDice4Val`: Cuarto criterio de desagregación en valenciano.
- `districtCas`: Distrito en castellano.
- `districtVal`: Distrito en valenciano.
- `neighborhoodCas`: Barrio en castellano.
- `neighborhoodVal`: Barrio en valenciano.
- `sliceAndDiceValue1Cas`: Valor del primer criterio de desagregación en castellano.
- `sliceAndDiceValue1Val`: Valor del primer criterio de desagregación en valenciano.
- `sliceAndDiceValue2Cas`: Valor del segundo criterio de desagregación en castellano.
- `sliceAndDiceValue2Val`: Valor del segundo criterio de desagregación en valenciano.
- `sliceAndDiceValue3Cas`:Valor del tercer criterio de desagregación en castellano.
- `sliceAndDiceValue3Val`:Valor del tercer criterio de desagregación en valenciano.
- `sliceAndDiceValue4Cas`: Valor del cuarto criterio de desagregación en castellano.
- `sliceAndDiceValue4Val`: Valor del cuarto criterio de desagregación en valenciano.
- `sourceCas`: Una secuencia de caracteres que proporciona la fuente original de los datos de la entidad en castellano.
- `sourceVal`: Una secuencia de caracteres que proporciona la fuente original de los datos de la entidad en valenciano.

## Atributos que establece la Oficina Técnica de Proyectos, de uso interno
- `calculationFrequencyId`: Identificador numérico del atributo calculationFrequency. 
- `kpiInternalIdentifier`: Identificador interno del indicador en forma de texto. Lo establece el equipo de la OTP.
- `kpiType`: Tipo del KPI. Posibles valores: Servicio o Ciudad. Lo establece el equipo de la OTP
- `internationalStandard`: ISO, ITU, etc.
- `measureUnitId`: Id de la unidad de medida.
- `operationalStatusNoDataIgnore`: Indica si debemos ignorar el valor del atributo operationalStatus. Posibles valores: true, false.
- `serviceId`: Id del servicio.

## Atributos específicos de ciertos desarrollos

### Atributos utilizados para CdmCiutat (EMT,Trafico...)
- `diaSemana`: Indica el día de la semana de la ultima medición. Posibles valores: L,M,X,J,V,S,D.

### Atributos utilizados para Valencia al Minut (EMT,Trafico...)
- `nameShort`: El nombre de este artículo pero más abreviado. Este es utilizado para mostrar el nombre de las leyendas de las gráficas cuando las visualizamos en formato móvil.

### Atributos utilizados para Residuos 
- `calculatedBy`: Indica que entidad realiza el cálculo.
- `calculationMethod`: Indica de que forma se realiza el cálculo (Ej: Manual).
- `denominatorValue`: Equivalente a kpiDenominatorValue.
- `numeratorValue`: Equivalente a kpiNumeratorValue.
- `process`: Equivalente a processName.

### Atributos utilizados para Estadística
- `numberIndicator`: Número de Indicadores.
- `numberMeta`: Número de Metas.
- `numberOds`: Número de Ods.
- `observations1Cas`: Observaciones en castellano.
- `observations1Val`: Observaciones en valenciano.
- `observations2Cas`: Observaciones en castellano.
- `observations2Val`: Observaciones en valenciano. 

### Atributos utilizados para Tráfico
- `isAggregated`: Indica si el valor esta agregado o no. Posibles valores: S o N.
- `measurelandUnit`: Equivalente a measureUnit.
- `owner`: Equivalente a serviceOwner.
- `ownerEmail`: Equivalente a serviceOwnerEmail.
- `tramo`: Descripcion del tramo.
